<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = 2, max = 255, minMessage="Le titre doit avoir au minimum 2 caractères et au maximum 255 caractères.")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min = 5, minMessage="La description doit avoir au minimum 5 caractères.")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sales_number;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     * @Assert\Url()
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=Genre::class, inversedBy="games")
     */
    private $Genre;

    /**
     * @ORM\ManyToMany(targetEntity=Platform::class, inversedBy="games")
     */
    private $Plateform;

    /**
     * @ORM\OneToMany(targetEntity=UserGame::class, mappedBy="game", orphanRemoval=true)
     */
    private $userGame;

    public function __construct()
    {
        $this->Genre = new ArrayCollection();
        $this->Plateform = new ArrayCollection();
        $this->userGame = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getSalesNumber(): ?int
    {
        return $this->sales_number;
    }

    public function setSalesNumber(?int $sales_number): self
    {
        $this->sales_number = $sales_number;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenre(): Collection
    {
        return $this->Genre;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->Genre->contains($genre)) {
            $this->Genre[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->Genre->removeElement($genre);

        return $this;
    }

    /**
     * @return Collection|Platform[]
     */
    public function getPlateform(): Collection
    {
        return $this->Plateform;
    }

    public function addPlateform(Platform $plateform): self
    {
        if (!$this->Plateform->contains($plateform)) {
            $this->Plateform[] = $plateform;
        }

        return $this;
    }

    public function removePlateform(Platform $plateform): self
    {
        $this->Plateform->removeElement($plateform);

        return $this;
    }

    /**
     * @return Collection|UserGame[]
     */
    public function getUserGame(): Collection
    {
        return $this->userGame;
    }

    public function addUserGame(UserGame $userGame): self
    {
        if (!$this->userGame->contains($userGame)) {
            $this->userGame[] = $userGame;
            $userGame->setGame($this);
        }

        return $this;
    }

    public function removeUserGame(UserGame $userGame): self
    {
        if ($this->userGame->removeElement($userGame)) {
            // set the owning side to null (unless already changed)
            if ($userGame->getGame() === $this) {
                $userGame->setGame(null);
            }
        }

        return $this;
    }
}
