<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\User;
use App\Form\GameType;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GameController extends AbstractController
{
    /**
     * @Route("/game", name="game")
     */
    public function game(Request $request, GameRepository $repo, PaginatorInterface $paginator): Response
    {
        
        $games = $repo->findAll();

        $games = $paginator->paginate(
            $games, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'games' => $games
        ]);
    }

    /**
     * @Route("/game/new", name="game_new")
     * @Route("/game/edit/{id}", name="game_edit")
     */
    public function form(Game $game = null, Request $request): Response
    {
        $user = $this->getUser();
        //Gestion de la création et de la mise à jour des jeux
        if(!$game){                 //si c'est la création du jeu
            $game = new Game();
        }
        
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            if(!$game->getId()){
                $game->setCreatedAt(new \DateTime());
            }
            $em = $this->getDoctrine()->getManager();

            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('game_show', ['id' => $game->getId()]);
        }

        return $this->render('game/new.html.twig', [
            "form" => $form->createView(),
            "editMode" => $game->getId() !== null
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render('game/home.html.twig');
    }

    /**
     * @Route("/game/{id}", name="game_show")
     */
    public function show(Game $game){
        
        
        return $this->render('game/show.html.twig', [
            'game' => $game
        ]);
    }

    /**
     * @Route("/game/delete/{id}", name="game_delete")
     */
    public function delGame(GameRepository $gameRepository, $id)
    {
        $game = $gameRepository->findOneById($id);

        if (!is_null($game)) {
            $em=$this->getDoctrine()->getManager();
            $em->remove($game);
            $em->flush();

            $this->addFlash(
                'notice',
                'Your changes were saved!'
            );
        }

        return $this->redirectToRoute('game');
    }
    
}