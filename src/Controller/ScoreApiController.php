<?php

namespace App\Controller;


use App\Entity\Score;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class ScoreApiController extends AbstractController
{
    /**
     * @Route("/score/api", name="score_api_get", methods={"GET"} )
     */
    public function index(ScoreRepository $repo): Response
    {
        $scores = $repo->findAll();
        //dump($users);

        return $this->json($scores);

    }

    /**
     * @Route("/score/api/new", name="score_api_post", methods={"POST"} )
     */
    public function apiNewScore(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, 
    ValidatorInterface $validator)
    {
        $jsonRecu = $request->getContent();
        
        try{
            $score = $serializer->deserialize($jsonRecu, Score::class, 'json');
            
            $errors = $validator->validate($score);
            if(count($errors) > 0){
                return $this->json($errors, 400);
            }

            $em->persist($score);
            $em->flush();
            
            return $this->json($score, 201);
        } catch(NotEncodableValueException | InvalidArgumentException $e){
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

}
