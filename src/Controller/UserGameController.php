<?php

namespace App\Controller;

use App\Entity\UserGame;
use App\Form\UserGameType;
use App\Repository\UserGameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/game")
 */
class UserGameController extends AbstractController
{
    /**
     * @Route("/", name="user_game_index", methods={"GET"})
     */
    public function index(UserGameRepository $userGameRepository): Response
    {
        return $this->render('user_game/index.html.twig', [
            'user_games' => $userGameRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_game_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $userGame = new UserGame();
        $form = $this->createForm(UserGameType::class, $userGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userGame->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userGame);
            $entityManager->flush();

            return $this->redirectToRoute('user_game_index');
        }

        return $this->render('user_game/new.html.twig', [
            'user_game' => $userGame,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_game_show", methods={"GET"})
     */
    public function show(UserGame $userGame): Response
    {
        return $this->render('user_game/show.html.twig', [
            'user_game' => $userGame,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_game_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserGame $userGame): Response
    {
        $form = $this->createForm(UserGameType::class, $userGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_game_index');
        }

        return $this->render('user_game/edit.html.twig', [
            'user_game' => $userGame,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_game_delete", methods={"POST"})
     */
    public function delete(Request $request, UserGame $userGame): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userGame->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userGame);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_game_index');
    }
}
