<?php

namespace App\Controller\Admin;

use App\Entity\Game;
use App\Entity\Genre;
use App\Form\GameAdminType;
use App\Form\GenreAdminType;
use App\Repository\GameRepository;
use App\Repository\GenreRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin")
 * Class AdminController
 * @package App\Controller\Admin
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/home", name="admin_home")
     */
    public function admin()
    {
        return $this->render("admin/home.html.twig");
    }

    /**
     * @Route("/game", name="admin_game")
     */
    public function game(Request $request, GameRepository $repo, PaginatorInterface $paginator): Response
    {
        
        $games = $repo->findAll();

        $games = $paginator->paginate(
            $games, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            6/*limit per page*/
        );

        return $this->render('admin/game.html.twig', [
            'games' => $games
        ]);
    }

    /**
     * @Route("/new", name="admin_game_new")
     * @Route("/edit/{id}", name="admin_game_edit")
     */
    public function form(Game $game = null, Request $request): Response
    {
        //Gestion de la création et de la mise à jour des jeux
        if(!$game){                 //si c'est la création du jeu
            $game = new Game();
        }
        
        $form = $this->createForm(GameAdminType::class, $game);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
            if(!$game->getId()){
                $game->setCreatedAt(new \DateTime());
            }
            $em = $this->getDoctrine()->getManager();

            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('admin_game_show', ['id' => $game->getId()]);
        }

        return $this->render('admin/new.html.twig', [
            "form" => $form->createView(),
            "editMode" => $game->getId() !== null
        ]);
    }

    /**
     * @Route("/game/{id}", name="admin_game_show")
     */
    public function show(Game $game){
        
        
        return $this->render('admin/show.html.twig', [
            'game' => $game
        ]);
    }

    /**
     * @Route("/game/delete/{id}", name="admin_game_delete")
     */
    public function deleteGame(GameRepository $gameRepository, $id)
    {
        $game = $gameRepository->findOneById($id);

        if (!is_null($game)) {
            $em=$this->getDoctrine()->getManager();
            $em->remove($game);
            $em->flush();

            $this->addFlash(
                'notice',
                'Your changes were saved!'
            );
        }

        return $this->redirectToRoute('admin_game');
    }

     /**
     * @Route("/genre/new", name="admin_genre_new")
     * @Route("/genre/edit/{id}", name="admin_genre_edit")
     */
    public function form2(Genre $genre = null, Request $request): Response
    {
        //Gestion de la création et de la mise à jour des jeux
        if(!$genre){                 //si c'est la création du jeu
            $genre = new Genre();
        }
        
        $form2 = $this->createForm(GenreAdminType::class, $genre);
        $form2->handleRequest($request);
        
        if($form2->isSubmitted() && $form2->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($genre);
            $em->flush();

            return $this->redirectToRoute('admin_genre', ['id' => $genre->getId()]);
        }

        return $this->render('admin/genre/new.html.twig', [
            "form" => $form2->createView(),
            "editMode" => $genre->getId() !== null
        ]);
    }

    /**
     * @Route("/genre", name="admin_genre")
     */
    public function genre(Request $request, GenreRepository $repo, PaginatorInterface $paginator): Response
    {
        
        $genres = $repo->findAll();
        
        return $this->render('admin/genre/genre.html.twig', [
            'genres' => $genres
        ]);
    }
}