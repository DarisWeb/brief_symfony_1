<?php

namespace App\DataFixtures;

use App\Entity\Game;
use App\Entity\Genre;
use App\Entity\Platform;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 20; $i++) {
            $game = new Game();
           
            $game->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true));
            $game->setDescription($faker->paragraph($nbSentences = 3, $variableNbSentences = true) );
            $game->setCreatedAt($faker->dateTime('now'));
            $game->setSalesNumber($faker->randomDigit );
            $game->setImage($faker->url);
            $manager->persist($game);
        }

        $manager->flush();

        for ($i = 0; $i < 20; $i++) {
            $genre = new Genre();
           
            $genre->setTitle($faker->sentence($nbWords = 3, $variableNbWords = true));
            $genre->setDescription($faker->paragraph($nbSentences = 3, $variableNbSentences = true) );
            $manager->persist($genre);
        }

        $manager->flush();

        for ($i = 0; $i < 20; $i++) {
            $platform = new Platform();
           
            $platform->setName($faker->sentence($nbWords = 3, $variableNbWords = true));
            $platform->setDescription($faker->paragraph($nbSentences = 6, $variableNbSentences = true) );
            $platform->setLogo('logo test');
            $manager->persist($platform);
        }

        $manager->flush();

    }
}
